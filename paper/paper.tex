\documentclass[12pt]{article}

\usepackage{arxiv}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}    % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsthm, amsmath, amsfonts} 
\usepackage{color}

\usepackage{algpseudocode,algorithm,algorithmicx}

\usepackage{cleveref}  
% blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{doi}
\usepackage[hyperref=true,doi=false,isbn=false,url=false,backend=biber,sorting=nyt,maxnames=99]{biblatex}
\addbibresource{bibliography.bib}
\addbibresource{id_papers.bib}
\addbibresource{gb_papers.bib}
\addbibresource{ml_algebra.bib}
\newtheorem{definition}{Definition}
\newtheorem{claim}{Claim}
\theoremstyle{definition}
\newtheorem{example}{Example}

\newcommand*\diff{\mathop{}\!\mathrm{d}}
\title{Weight Ordering Algorithm for \\Groebner Basis Computation in Identifiability Problems}

\date{\today}

\author{Mariya Bessonov\\
    CUNY NYC College of Technology,\\
    Department of Mathematics,\\
    New York, USA
    \And Ilia Ilmer\\
    Ph.D. Program in Computer Science,\\
    CUNY Graduate Center,\\
    New York, USA,\\
    \texttt{iilmer@gradcenter.cuny.edu}\\
    \And
    Tatiana Konstantinova\\
    Department of Mathematics,\\
    CUNY Queens College,\\
    New York, USA
    \And Alexey Ovchinnikov\\
    Department of Mathematics,\\
    CUNY Queens College\\
    Ph.D. Programs in Mathematics and Computer Science,\\
    CUNY Graduate Center,\\
    New York, USA\\
    \texttt{aovchinnikov@qc.cuny.edu}\\
    \And Gleb Pogudin\\LIX, CNRS,\\
    École Polytechnique,\\
    Institute Polytechnique de Paris, France\\
    \texttt{gleb.pogudin@polytechnique.}}

% Uncomment to remove the date
%\date{}

% Uncomment to override  the `A preprint' in the header
%\renewcommand{\headeright}{Technical Report}
%\renewcommand{\undertitle}{Technical Report}
% \renewcommand{\shorttitle}{\textit{arXiv} Template}

%%% Add PDF metadata to help others organize their library
%%% Once the PDF is generated, you can check the metadata with
%%% $ pdfinfo template.pdf

\begin{document}
\maketitle

\begin{abstract}
	Abstract
\end{abstract}


% keywords can be removed
\keywords{First keyword \and Second keyword \and More}


\section{Introduction}

Structural parameter identifiability is a well-known challenge that is crucial for designing quality mathematical models of real-world phenomena with ordinary differential equations. Identifiability problem arises when one needs to find a value for a particular parameter of the model. There can be either at most finitely many such  values (\emph{local} structural identifiability) or the value is unique (\emph{global} structural identifiability). For each case, there exist algorithms that answer these questions with high degree of correctness.

In this work, we address computational challenges that arise from Structural Identifiability Analyzer (SIAN)~\cite{hong_sian_2019,hong_global_2020}, the Monte-Carlo solution to the global identifiability problem. The core of SIAN relies on computing Gr\"obner basis~\cite{buchberger_theoretical_1976} of a polynomial system obtained from ODE model. This computation is well-known to be lengthy and resource-intensive for some polynomial systems despite such developments as F4~\cite{faugere_new_1999} and F5~\cite{faugere_new_2002} algorithms. For a taxonomy of Gr\"obner basis algorithms, we refer to~\cite{eder_survey_2017} and references therein.

Algorithms for Gr\"obner basis depend on the order of variables~\cite{cox_ideals_2013}. For example, simple lexicographic ordering such as \(x>y>z\) can produce a result different from \(y>z>x\). The order of individual variables induces the order on monomials in the polynomial system. The most common and empirically reliable ordering of monomials is the so-called total-degree-reverse-lexicographic order ({\tt tdeg}). In this ordering, one must first compare the total degree of monomials breaking ties by considering the smallest degrees of each factor in the monomial. For example, consider monomials \(x^2y\) and \(yz^2\). The total degree is 3 for both, however, under the {\tt tdeg} order assuming \(x>y>z\), we will arrange them as\[x^2y>yz^2.\]

Weighted ordering contributes an additional layer of comparison to monomial orderings where a variable is first compared by the weight value multiplied by its exponent and then ties are broken lexicographically. For instance, assume that we have variables \(x, y\) such that lexicographically \(x>y\). Given the weight assignment \([x\rightarrow 2, y\rightarrow 1]\) will lead to the following comparison \(x^2>y^3>x\) since we first compare by weight. Consider the following motivating example:

\begin{equation}
    P:=\begin{cases}
        x_1^2x_3^4 + x_1x_2x_3^2x_5^2 + x_1x_2x_3x_4x_5x_7 + x_1x_2x_3x_4x_6x_8 + x_1x_2x_4^2x_6^2 + x_2^2x_4^4,\\
        x_2^6,\\
        x_1^6
    \end{cases}
\end{equation}
Computing the Gr\"obner basis of this system with {\tt tdeg}-order of \(x_1, x_2, x_3,x_4, x_5, x_6,x_7,x_8\) takes approximately 670 seconds of total CPU time and 26 minutes of total elapsed time as computed in Maple 2021 on MacBook Pro with 16 GB of RAM and 16-core M1 processor. Modifying the system by substituting \(x_8\rightarrow x_8^2\) results in approximately \emph{2 seconds} of CPU time and only about \emph{1 second} of total elapsed time

Our approach to weighted ordering stems from the observation that weight assignment is equivalent to substitution. Concretely, if we wish to assign weight \(w\) to variable \(x\) then we may substitute \(x\rightarrow x^w\) in the system keeping the {\tt tdeg} order and the lexicographic order the same.

The following work is organized as follows. In \Cref{relwork}, we overview related research works studying weighted ordering and Gr\"obner basis computation. In \Cref{mainres}, we present our main results, namely weight assignment procedure for state variables in \Cref{states} and parameters in \Cref{params} combined into a full algorithm in \Cref{fullalgo}. We finalize the work by presenting experimental results in \Cref{experiments} and conclusions and future directions in \Cref{conclusions}.

\section{Related Work}
\label{relwork}

\section{Main result}
\label{mainres}
In this section, we present two algorithms for obtaining weighted orderings that increase the speed of the Gr\"obner basis computation. Let us begin by defining a model in a state-space form which will be the input to the algorithm.

\begin{definition}[Model in the state-space form]	
	\normalfont{} A model in~\emph{the state-space form} is a system
	\begin{equation}~\mathbf{\Sigma} :=~\begin{cases}
			\mathbf{x}'   & =\mathbf{f}(\mathbf{x},~\boldsymbol\mu,~\mathbf{u}),~ \\
			\mathbf{y}    & =\mathbf{g}(\mathbf{x},~\boldsymbol\mu,~\mathbf{u}),~ \\
			\mathbf{x}(0) & =\mathbf{x}^\ast,
		\end{cases}
		\label{eq:ode}
	\end{equation}
	where~\(\mathbf{f}=(f_1,\dots, f_n)\) and~\(\mathbf{g}=(g_1,\dots,g_n)\) with~\(f_i=f_i(\mathbf{x},~\boldsymbol\mu,~\mathbf{u})\),~\(g_i=g_i(\mathbf{x},~\boldsymbol\mu,~\mathbf{u})\) are rational functions over the field of complex numbers~\(\mathbb{C}\).

	The vector~\(\mathbf{x}=(x_1,\dots,x_n)\) represents the time-dependent state variables and~\(\mathbf{x}'\) represents the derivative. The vector-function~\(\mathbf{u}=(u_1,\dots,u_s)\) represents the input variables. The~\(m\)-vector~\(\mathbf{y}=(y_1,\dots,y_n)\) represents the output variables. The vector~\(\boldsymbol\mu=(\mu_1,\dots,\mu_{\lambda})\) represents the parameters and~\(\mathbf{x}^\ast=(x_1^\ast,\dots,x_n^\ast)\) defines initial conditions of the model.
    \label{def1}
\end{definition}

The algorithm of SIAN \cite{hong_sian_2019} transforms the input system from the form of \Cref{eq:ode} to a collection of differential polynomials prior to assessing global identifiability. The assessment relies on the output of Gr\"obner basis computation. We therefore seek a weight assignment to each variable of the resulting polynomial system that gets passed into Gr\"obner basis computation algorithm. From the programming point of view, we achieve this (in {\sc Maple}, for instance) by substituting a variable \(x\) to be \(x^w\) where \(w\in\mathbb{N}_{+}\) is a positive integer. The algorithms presentted in this work can be used for choosing state variables and parameters from \Cref{eq:ode} such that if used with weighted ordering the computation of Gr\"obner basis is faster.

\subsection{Weight Assignment for State Variables}
\label{states}
\subsubsection{Breadth-First Differentiation}
We begin by considering weights for state variables only. Empirically, we observe that lower occurrence frequency of a state variable in the right-hand side of the ODE system (excluding output functions) leads to a more successful weight assignment. By success here we mean the weights' ability to reduce the Gr\"obner basis algorithm runtime.

To generate weights for state variables that takes advantage of this observation, we perform the differentiation of the output functions in a breadth-first fashion. That is, we go over each output function \(y_i\) and apply a Lie derivative operator \(\mathcal{L}\) defined as

\begin{equation}
	\mathcal{L}y = \frac{\partial{y}}{\partial{t}} + \sum\limits_{i=1}^{n}f_i\frac{\partial{y}}{\partial{x_i}}+\sum\limits_{j=1}^{s}\frac{\diff u_j}{\diff t}\frac{\partial{y}}{\partial{u_j}}
\end{equation}

{\color{red}Gleb: I would remove $\frac{\partial{y}}{\partial{t}}$ since it may cause confusion (we do not have explicit $t$-dependence for $t$).
Also, in order to be consistent, I suggest we write $\dot{u}_j$, not $\frac{\diff u_j}{\diff t}$.}

If the substitution results in no previously unseen state variables appearing in the result, we skip the output function \(y_i\) in the next iteration. We repeat this until all functions are excluded.

At the same time we keep track of occurrences for each state variable. Roughly, we map each variable to a ``level'' value corresponding to a derivative order at which it appears. The ones that appear at later levels will have fewer occurrences in the right-hand side.

\begin{example}
	Consider the following ODE system with \(A,B,C\) being constants
	\[
		\Sigma=\begin{cases}
			\dot{x}_1 = Ax_1+Bx_2, \\
			\dot{x}_2 = Cx_1,      \\
			y_1 = x_1.
		\end{cases}
	\]
	Differentiating once:
	\[
		\dot{y}_1 = \dot{x}_1 = Ax_1+Bx_2.
	\]
	We see that the function \(x_2)\) occurs after the first differentiation and hence will be the candidate for substitution. At the same time, state \(x_1)\) is at level 0 and hence will not be substituted.
\end{example}

At this point we have to choose the way to actually pick functions from the resulting state-level mappings. Consider the next example.

\begin{example}
	Below is an ODE system with \(A,B,C,D\) being constants
	\[
		\Sigma=\begin{cases}
			\dot{x_1} = Ax_1+Bx_2, \\
			\dot{x_2} = Cx_1+Dx_3, \\
			\dot{x_3} = E,\\
			y_1 = x_1.
		\end{cases}
	\]
	Differentiating once:
	\[
		\dot{y_1} = \dot{x_1} = Ax_1+Bx_2,
	\]
	and then once more
	\[
		\ddot{y}_1 = A\dot{x_1}+B\dot{x_2}=Ax_1+Bx_2+Cx_1+Dx_3
	\]
	The function \(x_3)\) occurs after the second differentiation, state \(x_2)\) occurs at the 1st one. We can have several options in substituting these state variables. For instance, we can choose the ones with occurrence level of at least 1 or the one with the highest occurrence level.
\end{example}

In our experiments, we used two types of state variable selection. The first type is based picking those states that occur at maximal possible level and taking only those among them that have the lowest frequency of appearing in the right-hand side of \(\Sigma \). % SUBS 1

The second approach allows a bit more control over what gets picked for a weighted ordering substitution.
\begin{itemize}
	\item[a] We can pick all functions starting with a specified level \(l\) (for instance, if \(l=1\) only those that appear on level \(\ell \geq 1\))
	\item[b] Or only those that appear on the deepest (maximal) level possible without correction for frequency. In practice, most often this way  the selection coincides with the first type.
\end{itemize}

\Cref{bfs_diff} describes this successive BFS-style Lie-differentiation procedure. We use the BFS-analogy here because every time we take a derivative of an output function \(\mathbf{y}_i\), it is akin to going on level deeper in a tree and observing new nodes. Here \(\textsc{ToDiff}\) allows us to check and assign whether a function needs to be differentiated, \(\textsc{Derivative}(a, b)\) takes a Lie derivative of input \(a\) with respect to symbol \(b\). Finally, \(\textsc{States}(X)\) lets us extract state variables from an equation (or a system of equation) \(X\). We treat many of these functions as a black box here because the exact definition may be language specific.

\begin{algorithm}
	\caption{BFS Differentiation}\label{bfs_diff}
	\begin{algorithmic}[1]
		\Procedure{BFSDiff}{$\Sigma$}
		\State{} \(\mathbf{y}\,\gets\,\textsc{OutputFunctions}(\Sigma)\)\Comment{Extract array of output functions, each has a label \(\textsc{ToDiff}\) set to \(\textsc{True}\)}
		\State{} \(m\gets len(\mathbf{y})\)
		\State{} \(s\gets len(\Sigma.parameters)+len(\Sigma.states)\)
		\State{} \(\mathbf{Vis}\,\gets\,\textsc{Table}(\mathbf{y})\)\Comment{Initialize hash-table with states from \(\mathbf{y}\) as ``level 0''}
		\State{} current\_level\(\,\gets\) 0
		\For{\(j\gets 1, s+1\)}
		\State{} current\_level\(\,\gets\) current\_level+1
		\For{\(i\gets 1, m\)}
		\If{\(\textsc{ToDiff}(\mathbf{y}[i])==\textsc{True}\)}
		\State{} \(D\gets \textsc{Derivative}(\mathbf{y}[i], t)\)
		\State{} \(candidate\_states \gets \textsc{States}(D)\)
		\If{Any(\(cadidate\_states~\mathbf{in}~\mathbf{Vis})\)}
		\State{} \(\textsc{ToDiff}(\mathbf{y}[i])\gets \textsc{True}\)
		\Else
		\State{} \(\textsc{ToDiff}(\mathbf{y}[i])\gets \textsc{False}\) \EndIf
		\For{each \textbf{in} \(candidate\_states\)}
		\State{} \(\mathbf{Vis}[\text{each}]\gets\)current\_level
		\EndFor
		\EndIf
		\EndFor
		\If{\(\textsc{Sum}(\textsc{ToDiff}(\mathbf{y}[l]), l\gets 1, m)==0\)}
		\State \textsc{Break}
		\EndIf
		\EndFor
		\State \textbf{return} \(\mathbf{Vis}\)
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\subsubsection{Collecting Cancelling Monomials}
The next step after differentiation stems from an observation we made that in a given input system \( \Sigma \), the effect of a  weight assignment to state \(x_i\) can be enhanced by considering terms of type \(x_{i}x_{j}\) in the right-hand side of \( \Sigma \) such that there is a term \(-x_{i}x_{j}\) in another equation of the right-hand side. These are common in epidemic models and in models of chemical reaction. Consider the following illustrative example

\begin{example}
	Let the system be
	\[
		\Sigma=\begin{cases}
			\dot{x_1} = Ax_1+Bx_3, \\
			\dot{x_2} = -Cx_3x_2,  \\
			\dot{x_3} = Cx_3x_2\\
			y_1 = x_1.
		\end{cases}
	\]
	Differentiating once,
	\[
		\dot{y_1}=Ax_1+Bx_3
	\]
	and then once more
	\[
		\ddot y_1=Ax_1+Bx_3 + Cx_3x_2
	\]
	we see that \(x_3\) appears on level 1, and \(x_2\) appears on level 2. Even if we were to take the deepest level, i.e. \(x_2\), because we have both \(\pm Cx_2x_3\) occur in our system we will include \(x_3\) in the list of states for which we assign weights.
\end{example}
The procedure for collecting states from these types of terms will be denoted by \[\textsc{States}(\textsc{CollectCancellingTerms}(\Sigma, \textbf{Vis})).\] This is only used in the case where we do not explicitly count frequency (see earlier for explanation of two algorithm types).

\subsection{Weights for Parameters}
\label{params}

To take maximal advantage of the weighted order, we seek similar weight assignments for constant parameters. From our experiments, we observe that the weight assignment for constants works best when we collect such constants \(C\) that occur in terms of type \(Cx\) where \(x\) is such a state variable that occurs in the right-hand side of output functions \(\mathbf{y}\). Moreover, the constants should be locally identifiable.

In addition to the rule above, we accept constants that occur in similar form \(Cx\) in the right-hand side of \(\mathcal{y}\).

\begin{example}
	Consider the system

	\[
		\Sigma=\begin{cases}
			\dot{x_1}=Ax_1+Bx_2+C, \\
			\dot{x_2}=x_2,         \\
			y=T*x1.
		\end{cases}
	\]
	The constants we choose here are \(A, B, T\) because \(A\) and \(B\) occur in the desired for of the right-hand side of the ODE system. The constant \(T\) arises from the equation \(y=Tx_1+x_2\).
\end{example}

Let us denote the procedure that obtains substitution candidates among constant parameters in the way we described by \(\textsc{Parameters}(\Sigma)\)

\subsection{Full Algorithm}
\label{fullalgo}

The full algorithms to generate a collection of parameters and states in a given ODE system is presented below. This collection will be used to assign weights at the time of Gr\"obner basis computation. Recall that we ultimately had two versions: based on frequency and based on level. First, listing \Cref{freq_weights} shows the procedure for obtaining states and parameters using frequency information with BFS-differentiation.

\begin{algorithm}
	\caption{Generate Weighetd Ordering by Frequency.\\The procedure accepts the input ODE system \(\Sigma\)}\label{freq_weights}
	\begin{algorithmic}[1]
		\Procedure{GetWeightedOrderingFreq}{$\Sigma$}
		\State \(\mathbf{Vis} \gets \textsc{BFSDiff}(Sigma)\)\Comment{Obtain our hash table}
		\State \(\text{Result}\,\gets\,\textsc{Parameters}(\Sigma)\)\Comment{All parameters for which we assign weight}
		\State \(m\, \gets\, \textsc{Max}(\mathbf{Vis})\)\Comment{Get maximal possible level}
		\State \(Counts\gets \textsc{HashTable}(\Sigma.states, Int)\)\Comment{Get a counting hash table for each state}
		\For{each state \(S\) \textbf{in} \(\Sigma.states\)}
		\If{\(\textbf{Vis}[S]!=\textsc{Nil}~\&\&~\textbf{Vis}[S]\geq m\)}
		\State \(Counts[S]\gets Counts[S]+1\)
		\EndIf{}
		\EndFor{}
		\State \(min\_count\gets \textsc{Min}(Counts)\)
		\State \(\text{Result}\,\gets\,S\) where \(Counts[S]\leq min\_count\)
		\State \textbf{return} Result
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

In the next listing, \Cref{weights_table} contains present the procedure that returns states and parameters using a different approach relying only on the BFS-differentiation outcome and level information.

\begin{algorithm}
	\caption{Generate Weighetd Ordering.\\The procedure accepts the input ODE system \(\Sigma\)}\label{weights_table}
	\begin{algorithmic}[1]
		\Procedure{GetWeightedOrdering}{$\Sigma,\,min\_level,\,strict$}
		\State \(\mathbf{Vis} \gets \textsc{BFSDiff}(Sigma)\)\Comment{Obtain our hash table}
		\State \(\text{Result}\,\gets\,\textsc{States}(\textsc{CollectCancellingTerms}(\Sigma, \textbf{Vis}))\)\Comment{At this point Result contains all state variables for which we assign weight \(\mathbf{w}\)}
		\State \(\text{Result}\,\gets\,\textsc{Parameters}(\Sigma)\)
		\If{\(strict == \textsc{False}\)}
		\State Filter Result for only those states \(S\) that occur at \(\mathbf{Vis}[S]\geq min\_level\)
		\Else
		\State Filter Result for only those states \(S\) that occur at max level in \(\mathbf{Vis}\)
		\EndIf
		\State \textbf{return} Result
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

Assume that we now obtained, using any of the algorithms presented here, a collection \(\Psi\) of states and parameters for which we would like to assign weights. The actual assignment then follows the following general pattern:
\begin{itemize}
	\item For states, each state \(x_i\) and \emph{all of its derivatives} that occur in SIAN's polynomial system get a weight \(w_i\).
	\item For parameters, each parameter \(\theta_j\) gets its respective weight \(w_j\).
\end{itemize}
\section{Experimental Results}
\label{experiments}
In this section we present several examples of ODE systems, for which we observe reduction in both the runtime and memory. All simulations were run on a cluster with 64 Intel Xeon CPU with 2.30GHz clock frequency and 755 GB RAM. We ran the computation using two computer algebra systems, {\sc Maple} and Magma. By default, all weights we assign are equal 2 unless otherwise specified.

\subsection{Cholera Model}

In this example, we consider the following state-space system describing the dynamics of cholera~\cite[Eq. 3]{lee_model_2017}. The parameters and states selected by \Cref{freq_weights} are presented below:\[
	\mu \rightarrow 2, \alpha_l \rightarrow 2, w \rightarrow 2, \gamma \rightarrow 2, k \rightarrow 2.
\]
Note that the only state selected in \(w\) and for this state, we assign weight of 2 for each derivative occurring in the polynomial system of SIAN. Notice that this is the only state occurring on level 1 after BFS-differentiation.

At the same time, \Cref{weights_table} with minimum level 1 and { \tt strict=true} returns \[
	s \rightarrow 2, \mu \rightarrow 2, \alpha_l \rightarrow 2, w \rightarrow 2, \gamma \rightarrow 2, k \rightarrow 2.
\]
Notice that a new state \(s\) appears in the list and, as before, all derivatives that appear in the polynomial system generated by SIAN will have a weight assigned to them based on the table above.
\begin{table}[!ht]
	\centering
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (CPU sec) & \Cref{freq_weights} (CPU sec) & \Cref{weights_table} (CPU sec) \\\hline\hline
		51          & 45        & 401.74             & {\bf 72.88}                   & {\bf 58.33}                    \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (Real sec) & \Cref{freq_weights} (Real sec) & \Cref{weights_table} (Real sec) \\\hline\hline
		51          & 45        & 53.34               & {\bf 7.99}                     & {\bf 6.68}                      \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (MB) & \Cref{freq_weights} (MB) & \Cref{weights_table} (MB) \\\hline\hline
		51          & 45        & 971.16        & {\bf 172.98}             & {\bf 164.10}              \\\hline
	\end{tabular}
	\caption{Results of applying the weighted ordering to the model in \Cref{cholera}}
	\label{cholera_tables}
\end{table}
In \cref{cholera_tables}, we show benchmarking results of the no-weight baseline and weight assignment outcomes. We observe significant runtime reduction.

\begin{equation}
	\label{cholera}
	\begin{cases}
		s' = \mu - \beta_i\, s \, i - \beta_w  s  w - \mu  s + \alpha_l r,  \\
		i' = \beta_w \, s \, w + \beta_i \, s \,i - \gamma \, i - \mu \, i, \\
		w' = \xi \, (i - w),                                                \\
		r' = \gamma \, i - \mu \, r - \alpha \, r,                          \\
		y_1 = k\,i,                                                         \\
		y_2 = i + r + s
	\end{cases}
\end{equation}

\subsection{Goodwin Oscillator}

The following model in \Cref{goodwin} comes from the work by Goodwin~\cite{goodwin_oscillatory_1965} and describes time periodicity in cell behavior.
\begin{equation}
	\begin{cases}
		x_1' = -b \, x_1 + \frac{1}{(c + x_4)},                              \\
		x_2' = \alpha \, x_1 - \beta \, x_2,                                 \\
		x_3' = \gamma \, x_2 - \delta \, x_3,                                \\
		x_4' = \frac{\sigma \, x_4 \, (\gamma \, x_2 - \delta \, x_3)}{x_3}, \\
		y = x_1
	\end{cases}
	\label{goodwin}
\end{equation}
% TODO: show Goodwin Oscillator example.

We present the results of \Cref{freq_weights} and \Cref{weights_table} in \cref{goodwin_tables}. We see a significant improvement of up to 14 times in the Gr\"obner basis computation time and over 3-fold improvement in memory usage.
% Furthermore, we can get further improvement if we increase the weight by 1, see

In the case of \Cref{freq_weights}, the selection of variables is \[
	x_3 \rightarrow 2, z_{aux}\rightarrow 2.
\]
In addition to the state, SIAN uses an auxiliary variable \(z_{aux}\) to account for presence of denominators in the right-hand side of the original input ODE system. We observe that giving a weight of 2 to this variable leads to improvement, hence additional parameter weight assignment here.

\Cref{weights_table} results in
\[
	x_2\rightarrow 2, x_3 \rightarrow 2, z_{aux}\rightarrow 2.
\]

\begin{table}[!ht]
	\centering
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (CPU sec) & \Cref{freq_weights} (CPU sec) & \Cref{weights_table} (CPU sec) \\\hline\hline
		42          & 44        & 9702.32            & {\bf 2979.11}                 & {\bf 707.64}                   \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (Real sec) & \Cref{freq_weights} (Real sec) & \Cref{weights_table} (Real sec) \\\hline\hline
		42          & 44        & 1352.23             & {\bf 393.06}                   & {\bf 99.27}                     \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (GB) & \Cref{freq_weights} (GB) & \Cref{weights_table} (GB) \\\hline\hline
		42          & 44        & 10.99         & {\bf 3.20}               & {\bf 3.21 }               \\\hline
	\end{tabular}
	\caption{Results of applying the weighted ordering to the model in \Cref{goodwin}.}
	\label{goodwin_tables}
\end{table}

The measurements with weight 2 are presented in \Cref{goodwin_tables}. We create an additional run to illustrate the dramatic effect the value of the weight with our choice or parameter can have on the runtime. In \Cref{goodwin_tables_deg3}, we assign weight of 3 to selected variables. Notice that in the case of \Cref{weights_table} (last column od the table), we observe almost 10-fold improvement in CPU time and real (elapsed) time. Given these improvements, one can run \Cref*{goodwin} in SIAN using only a laptop.

\begin{table}[!ht]
	\centering
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (CPU sec) & \Cref{freq_weights} (CPU sec) & \Cref{weights_table} (CPU sec) \\\hline\hline
		42          & 44        & 9702.32            & {\bf 2362.76}                 & {\bf 128.85}                   \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (Real sec) & \Cref{freq_weights} (Real sec) & \Cref{weights_table} (Real sec) \\\hline\hline
		42          & 44        & 1352.23             & {\bf 269.14}                   & {\bf 15.92}                     \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (GB) & \Cref{freq_weights} (GB) & \Cref{weights_table} (GB) \\\hline\hline
		42          & 44        & 10.99         & {\bf 3.19}               & {\bf 2.63 }               \\\hline
	\end{tabular}
	\caption{Results of applying the weighted ordering to the model in \Cref{goodwin} with weight 3.}
	\label{goodwin_tables_deg3}
\end{table}


\subsection{SEIR COVID-19 Model}
% SEIQRDC model

In this final example, we present a SEIR model of epidemics~\cite[table 2, ID=14]{massonis_structural_2020}. The example originally had 3 output functions, however, we reduced it to one to create more of a computational challenge for out program. We also use the term \(\mu\,i\,s\) instead of \(\mu\,i\mu\,s\) in the third equation. The state-space form of the model is presented in \Cref{seiqrdc}.

\begin{equation}
	\begin{cases}
		S' = \mu \, N - \alpha \, S - \beta \, S \, I \, N - \mu \, S, \\
		E' = \beta \, S \, I \, N - \mu \, E - \gamma \, E,            \\
		I' = \gamma \, E - \delta \, I - \mu \, I \, S,                \\
		Q' = \delta \, I - \lambda \, Q - \kappa \, Q - \mu \, Q,      \\
		R' = \lambda \, Q - \mu \, S,                                  \\
		D' = \kappa \, Q,                                              \\
		C' = \alpha \, S - \mu \, C - \tau \, C ,                      \\
		y = C
	\end{cases}
	\label{seiqrdc}
\end{equation}

As a result of our weight ordering pick, we assign weights as follows
\[
	E \rightarrow 2, \tau \rightarrow 2, \mu \rightarrow 2.
\]
We observe a dramatic decrease in elapsed time by a factor of approximately 41 for both algorithms. The CPU time went down by a factor of over 100 and the memory usage was reduced from 15 to just over 2 GB.
\begin{table}[!ht]
	\centering
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (CPU sec) & \Cref{freq_weights} (CPU sec) & \Cref{weights_table} (CPU sec) \\\hline\hline
		51          & 51        & 100601.53          & {\bf 946.41}                  & {\bf 959.66}                   \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (Real sec) & \Cref{freq_weights} (Real sec) & \Cref{weights_table} (Real sec) \\\hline\hline
		51          & 51        & 6186.27             & {\bf 149.74}                   & {\bf 164.10}                    \\\hline
	\end{tabular}\\\vspace{0.5cm}
	\begin{tabular}{||c|c|c|c|c|c||}
		\hline
		Polynomials & Variables & Baseline (GB) & \Cref{freq_weights} (GB) & \Cref{weights_table} (GB) \\\hline\hline
		51          & 51        & 15.4          & {\bf 2.11}               & {\bf 2.11}                \\\hline
	\end{tabular}
	\caption{Results of applying the weighted ordering to the model in \Cref{seiqrdc}}
	\label{seiqrdc_tables}
\end{table}


% NOTE *_subs_1 = Freq
\section{Conclusion}
\label{conclusions}

\printbibliography{}
\end{document}
